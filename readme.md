## Codeassasin Linux Mint Global Composer requires.

- laravel/installer: ^1.3
- friendsofphp/php-cs-fixer: ^2.1
- cpriego/valet-linux: ^2.0
- phpunit/phpunit: ^6.0
